//
//  MyTest.m
//
#import <GHUnitIOS/GHUnit.h>
#import "ViewController.h"

@interface MyTest : GHTestCase
@end

@implementation MyTest

- (void)test {
    ViewController* vc = [[ViewController alloc] init];
    [vc launchThreads:MultithreadingType_NSOperationQueue];
}

@end
