//
//  Computer.h
//  GrandCentralPi
//
//  Created by Peter Prokop on 16/03/14.
//  Copyright (c) 2014 Prokop. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MultithreadingType_POSIX,
    MultithreadingType_NSThread,
    MultithreadingType_GCD,
    MultithreadingType_performSelector,
    MultithreadingType_NSOperationQueue
} MultithreadingType;

@interface Computer : NSObject

- (void)launchThreads:(MultithreadingType) type;

@end
