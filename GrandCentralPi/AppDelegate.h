//
//  AppDelegate.h
//  GrandCentralPi
//
//  Created by Peter Prokop on 13/03/14.
//  Copyright (c) 2014 Prokop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
