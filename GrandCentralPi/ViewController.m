//
//  ViewController.m
//  GrandCentralPi
//
//  Created by Peter Prokop on 13/03/14.
//  Copyright (c) 2014 Prokop. All rights reserved.
//

// Just in case: Pi is this bad boy:
// 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679

#import "ViewController.h"
#import "Computer.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    Computer* cmp = [[Computer alloc] init];
    
    [cmp launchThreads:MultithreadingType_NSOperationQueue];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
