//
//  main.m
//  GrandCentralPi
//
//  Created by Peter Prokop on 13/03/14.
//  Copyright (c) 2014 Prokop. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
