//
//  Computer.m
//  GrandCentralPi
//
//  Created by Peter Prokop on 16/03/14.
//  Copyright (c) 2014 Prokop. All rights reserved.
//

#import "Computer.h"

#include <assert.h>
#include <pthread.h>

#define NUM_ITERATIONS 1<<20
#define NUM_THREADS 1<<2

struct DataStruct {
    int begin;
    int end;
};

typedef struct DataStruct DataStruct;

static long double __pi = 0;
static int __numRunningThreads = 0;
MultithreadingType context;

// POSIX
pthread_mutex_t numRunningThreadsMutex;
pthread_cond_t numRunningThreadsCondition;

// NSThread
NSCondition* numRunningThreadsNSThreadsCondition;

// GCD
dispatch_group_t runningThreadsDispatchGroup;

// NSOperationQueue
NSOperation* finalOp;

@implementation Computer

#pragma mark -
#pragma mark Computing

#pragma mark POSIX API

void* threadWatcher(void* data) {
    while(1) {
        pthread_mutex_lock(&numRunningThreadsMutex);
        while(__numRunningThreads > 0) {
            pthread_cond_wait(&numRunningThreadsCondition, &numRunningThreadsMutex);
        }
        printf("Final Pi: %.20Lf \n", __pi);
        pthread_mutex_unlock(&numRunningThreadsMutex);
        return NULL;
    }
    return NULL;
}

// Compute Pi using Gregory–Leibniz series in embarrassingly parallel variant
void* processDataChunk(void* data) {
    DataStruct* intData = data;
    int begin = intData->begin;
    int end = intData->end;
    
    printf("thread begin end: %i %i \n", begin, end);
    
    long double total = 0;
    long double summand = 0;
    
    for(int n=begin; n<=end; n++) {
        summand = 4./(2*n+1);
        if(n%2)
            summand = -summand;
        total += summand;
    }
    
    printf("pi: %.20Lf \n", __pi);
    __pi += total;
    printf("pi: %.20Lf \n", __pi);
    
    free(data);
    
    if(context == MultithreadingType_POSIX) {
        pthread_mutex_lock(&numRunningThreadsMutex);
        __numRunningThreads--;
        if(__numRunningThreads == 0)
            pthread_cond_signal(&numRunningThreadsCondition);
        pthread_mutex_unlock(&numRunningThreadsMutex);
    } else if (context == MultithreadingType_NSThread) {
        [numRunningThreadsNSThreadsCondition lock];
        __numRunningThreads--;
        if(__numRunningThreads == 0)
            [numRunningThreadsNSThreadsCondition signal];
        [numRunningThreadsNSThreadsCondition unlock];
    } else {
        __numRunningThreads--;
    }
    
    return NULL;
}

#pragma mark NSThread

- (void)threadWatcher {
    [numRunningThreadsNSThreadsCondition lock];
    while(__numRunningThreads > 0) {
        [numRunningThreadsNSThreadsCondition wait];
    }
    printf("Final Pi: %.20Lf \n", __pi);
    [numRunningThreadsNSThreadsCondition unlock];
    return;
}

- (void)processTaskNSThread:(NSValue *)val {
    DataStruct *data = [val pointerValue];
    processDataChunk(data);
}

#pragma mark Launch threads

- (void)launchThreads:(MultithreadingType) type {
    context = type;
    __pi = 0.;
    int chunkSize = NUM_ITERATIONS/NUM_THREADS;
    printf("NUM_THREADS: %i \n", NUM_THREADS);
    printf("chunkSize: %i \n", chunkSize);
    int currentChunkBeginnig = 0;
    
    // No lock needed
    __numRunningThreads++;
    
    if(type == MultithreadingType_POSIX) {
        pthread_mutex_init(&numRunningThreadsMutex, NULL);
        pthread_cond_init(&numRunningThreadsCondition, NULL);
        
        pthread_attr_t  attr;
        pthread_t       posixThreadID;
        int             returnVal;
        
        returnVal = pthread_attr_init(&attr);
        assert(!returnVal);
        returnVal = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        assert(!returnVal);
        
        int threadError = pthread_create(&posixThreadID, &attr, &threadWatcher, NULL);
        
        returnVal = pthread_attr_destroy(&attr);
        assert(!returnVal);
        if (threadError != 0) {
            // Report an error.
        }
    } else if (type == MultithreadingType_NSThread) {
        numRunningThreadsNSThreadsCondition = [NSCondition new];
        
        NSThread* thread = [[NSThread alloc] initWithTarget:self
                                                   selector:@selector(threadWatcher)
                                                     object:nil];
        [thread start];
    } else if (type == MultithreadingType_GCD) {
        runningThreadsDispatchGroup = dispatch_group_create();
    } else if (type == MultithreadingType_NSOperationQueue) {
        finalOp = [NSBlockOperation blockOperationWithBlock:^{
            printf("Final Pi: %.20Lf \n", __pi);
        }];
    }
    
    for(int i=0; i<NUM_THREADS; i++) {
        // Allocate data on heap
        DataStruct* data = (DataStruct*) malloc(sizeof(DataStruct));
        data->begin = currentChunkBeginnig,
        data->end = currentChunkBeginnig+chunkSize;
        currentChunkBeginnig += chunkSize+1;
        
        __numRunningThreads++;
        
        switch (type) {
            case MultithreadingType_POSIX: {
                pthread_attr_t  attr;
                pthread_t       posixThreadID;
                int             returnVal;
                
                returnVal = pthread_attr_init(&attr);
                assert(!returnVal);
                returnVal = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
                assert(!returnVal);
                
                int threadError = pthread_create(&posixThreadID, &attr, &processDataChunk, data);
                
                returnVal = pthread_attr_destroy(&attr);
                assert(!returnVal);
                if (threadError != 0) {
                    // Report an error.
                }
                break;
            }
            case MultithreadingType_NSThread: {
                NSValue* val = [NSValue valueWithPointer:data];
                NSThread* thread = [[NSThread alloc] initWithTarget:self
                                                           selector:@selector(processTaskNSThread:)
                                                             object:val];
                [thread start];
                break;
            }
            case MultithreadingType_GCD: {
                dispatch_group_async(runningThreadsDispatchGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    processDataChunk(data);
                });
                
                break;
            }
            case MultithreadingType_performSelector: {
                NSValue* val = [NSValue valueWithPointer:data];
                [self performSelectorInBackground:@selector(processTaskNSThread:)
                                       withObject:val];
                break;
            }
            case MultithreadingType_NSOperationQueue: {
                NSOperationQueue *queue = [NSOperationQueue new];
                NSBlockOperation* bo = [NSBlockOperation blockOperationWithBlock:^{
                    processDataChunk(data);
                }];
                /* You can add dependencies for operations from the other queues
                 https://developer.apple.com/library/mac/documentation/cocoa/reference/NSOperationQueue_class/Reference/Reference.html
                 */
                [finalOp addDependency:bo];
                [queue addOperation:bo];
                
                break;
            }
                
            default:
                break;
        }
    }
    
    if(type == MultithreadingType_POSIX) {
        pthread_mutex_lock(&numRunningThreadsMutex);
        __numRunningThreads--;
        pthread_mutex_unlock(&numRunningThreadsMutex);
    } else if (type == MultithreadingType_NSThread) {
        [numRunningThreadsNSThreadsCondition lock];
        __numRunningThreads--;
        [numRunningThreadsNSThreadsCondition unlock];
    } else if (type == MultithreadingType_GCD) {
        dispatch_group_notify(runningThreadsDispatchGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            printf("Final Pi: %.20Lf \n", __pi);
        });
    } else if (type == MultithreadingType_NSOperationQueue) {
        [[NSOperationQueue mainQueue] addOperation:finalOp];
    } else {
        __numRunningThreads--;
    }
}

@end
